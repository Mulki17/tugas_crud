<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Http\Controllers\DB;
use Illuminate\Http\Request;

class ProductsController extends Controller
{

    public function index()
    {
        $product = Product::all();
        return view('product.index', compact('product'));
    }


    public function create()
    {
        return view('product.create');
    }


    public function store(Request $request)
    {
        $product = new Product;
        $product->name  = $request->name;
        $product->price = $request->price;
        $product->save();
        return redirect()->route('product.index');
    }


    public function show($id)
    {
        $product = Product::findOrFail($id);
        return view('product.show', compact('product'));
    }


    public function edit($id)
    {
        $product = Product::findOrFail($id);
        return view('product.edit', compact('product'));
    }


    public function update(Request $request, $id)
    {
        $product = Product::findOrFail($id);
        $product->name  = $request->name;
        $product->price = $request->price;
        $product->save();
        return redirect()->route('product.index');
    }


    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        $product->delete();
        return redirect()->route('product.index');
    }
}
